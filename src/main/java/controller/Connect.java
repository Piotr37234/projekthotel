package controller;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


@Named
public class Connect {

    //To jest Singleton umożliwa w "elegancki"
    //sposob na dostep do bazy danych

    //EntityManagerFactory factory ktore tworzy 1 raz
    //nawet gdy wywolujemy wiele razy createEntityManagerFactory


    private static EntityManagerFactory factory;
    private static Connect instance;
    public synchronized static Connect getConnect() {
        if (instance == null) {
            instance = new Connect();
        }
        return instance;
    }

    public EntityManagerFactory createEntityManagerFactory() {
        if (factory == null) {
            factory = Persistence.createEntityManagerFactory("NewPersistenceUnit");
        }
        return factory;
    }

    public EntityManager createEntityManager() {
        return this.createEntityManagerFactory().createEntityManager();
    }



    public void closeEntityManagerFactory() {
        if (factory != null) {
            factory.close();
        }
    }


}
