package controller;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

//uwaga to jest tylko "słuchacz" sam w sobie nie istnieje :)
//wykona tylko contextInitialized po uruchomieniu
//i contextDestroyed po wylaczeniu

@WebListener //to zaznacza ze jest to sluchacz
public class StartListener implements ServletContextListener{
    public void contextInitialized(ServletContextEvent sce) {
        Connect.getConnect().createEntityManagerFactory();
    }
    public void contextDestroyed(ServletContextEvent sce) {
        Connect.getConnect().closeEntityManagerFactory();
    }
}
