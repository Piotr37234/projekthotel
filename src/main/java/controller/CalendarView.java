package controller;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

@ManagedBean
public class CalendarView {
    private String message="Zalogowaleś się prawidłowo";
    public void click() {
        PrimeFaces.current().ajax().update("form:display");
        PrimeFaces.current().executeScript("PF('dlg').show()");
    }

    public void click1() {
        PrimeFaces.current().ajax().update("form1:display1");
        PrimeFaces.current().executeScript("PF('dlg1').show()");
    }
    public void click2() {
        PrimeFaces.current().ajax().update("form2:display2");
        PrimeFaces.current().executeScript("PF('dlg2').show()");
    }

}