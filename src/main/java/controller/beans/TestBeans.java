package controller.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class TestBeans {

    @ManagedProperty(value="#{param.x}")
    int tester;

    public int getTester() {
        return tester;
    }

    public void setTester(int tester) {
        this.tester = tester;
    }





}
