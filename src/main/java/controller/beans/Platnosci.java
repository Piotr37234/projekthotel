package controller.beans;

import controller.Connect;
import controller.managedEntity.GoscJPQLManager;
import controller.managedEntity.PokojJPQLManager;
import controller.managedEntity.RezerwacjaJPQLManager;
import model.Gosc;
import model.Rezerwacja;

import javax.persistence.EntityManager;
import java.util.List;

public class Platnosci {
    EntityManager em = Connect.getConnect().createEntityManager();
    GoscJPQLManager goscjpql = new GoscJPQLManager();
    PokojJPQLManager pokojpql = new PokojJPQLManager();
    RezerwacjaJPQLManager rezerwacjajpql = new RezerwacjaJPQLManager();



    public void zaplacZaRezerwacje(Gosc gosc) {
        List<Rezerwacja> lista = rezerwacjajpql.getListaRezerwacjiGoscia(gosc.getId());
        em.getTransaction().begin();
        for (Rezerwacja r : lista) {
            r.setRozliczona(true);
            em.merge(r);
        }
        em.getTransaction().commit();
        em.clear();
    }

    public void aktualizujRachunek(Gosc gosc) {
        double wynik = getObliczDoZaplaty(gosc);
        gosc.setRachunek(wynik);
        if (gosc.getRachunek() == 0) {
            gosc.setRozliczony(true);
        } else {
            gosc.setRozliczony(false);
        }
        em.getTransaction().begin();
        em.merge(gosc);
        em.getTransaction().commit();
        em.clear();
    }

    public double getObliczDoZaplaty(Gosc gosc) {
        //....
        return goscjpql.getZaplataZaRezerwacje(gosc.getId());
    }


}
