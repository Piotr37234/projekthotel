package controller.beans;

import controller.Connect;
import controller.managedEntity.GoscJPQLManager;
import controller.managedEntity.PokojJPQLManager;
import model.Gosc;
import model.Pokoj;
import model.Rezerwacja;

import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@ManagedBean
@RequestScoped
public class RezerwacjaBeans implements Serializable {


    @ManagedProperty(value="#{param.x}")
    private int idPokoju;
    private Date czaspoczatkowy;
    private Date czaskoncowy;
    private String email;
    private String imie;
    private String nazwisko;
   private int wiek;
    private String pesel;
    private String username;
    private String password;






    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public int getIdPokoju() {
        return idPokoju;
    }

    public void setIdPokoju(int idPokoju) {
        this.idPokoju = idPokoju;
    }

    public Date getCzaspoczatkowy() {
        return czaspoczatkowy;
    }

    public void setCzaspoczatkowy(Date czaspoczatkowy) {
        this.czaspoczatkowy = czaspoczatkowy;
    }

    public Date getCzaskoncowy() {
        return czaskoncowy;
    }

    public void setCzaskoncowy(Date czaskoncowy) {
        this.czaskoncowy = czaskoncowy;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }
}
