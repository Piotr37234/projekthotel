package controller.beans;

import java.util.Random;

public class GeneratorKodow {

    private static final String ZNAKI = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    private static final int DLUGOSC = 10;

    public String getLosowyKod(){

        StringBuffer randStr = new StringBuffer();
        for(int i = 0; i< DLUGOSC; i++){
            int number = getLosowaLiczba();
            char ch = ZNAKI.charAt(number);
            randStr.append(ch);
        }
        return randStr.toString();
    }


    private int getLosowaLiczba() {
        int randomInt = 0;
        Random rand = new Random();
        randomInt = rand.nextInt(ZNAKI.length());
        if (randomInt - 1 == -1) {
            return randomInt;
        } else {
            return randomInt - 1;
        }
    }

    public static void main(String a[]){
        GeneratorKodow msr = new GeneratorKodow();
        System.out.println(msr.getLosowyKod());
        System.out.println(msr.getLosowyKod());
        System.out.println(msr.getLosowyKod());
        System.out.println(msr.getLosowyKod());
        System.out.println(msr.getLosowyKod());
        System.out.println(msr.getLosowyKod());
        System.out.println(msr.getLosowyKod());
    }
}