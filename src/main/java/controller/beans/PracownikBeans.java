package controller.beans;

import controller.Connect;
import controller.managedEntity.PracownikPQLManager;
import model.Pracownik;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;

@ManagedBean
@RequestScoped
public class PracownikBeans {

    PracownikPQLManager pracownikjpql=new PracownikPQLManager();
    EntityManager em = Connect.getConnect().createEntityManager();

    private String imie;
    private String nazwisko;
    private int wiek;
    private String pesel;
    private String username;
    private String password;
    private String stanowisko;
    private double pensja;
    private double premia;

    public void dodajPracownika(){
        Pracownik nowy = new Pracownik();
        nowy.setImie(imie);
        nowy.setNazwisko(nazwisko);
        nowy.setPesel(pesel);
        nowy.setPensja(pensja);
        nowy.setPremia(premia);
        nowy.setStanowisko(stanowisko);
        nowy.setWiek(wiek);
        nowy.setUsername(username);
        nowy.setPassword(password);
    }


    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStanowisko() {
        return stanowisko;
    }

    public void setStanowisko(String stanowisko) {
        this.stanowisko = stanowisko;
    }

    public double getPensja() {
        return pensja;
    }

    public void setPensja(double pensja) {
        this.pensja = pensja;
    }

    public double getPremia() {
        return premia;
    }

    public void setPremia(double premia) {
        this.premia = premia;
    }


}
