package controller.beans;

import controller.Connect;
import controller.beans.zalogowany.SendMail;
import controller.managedEntity.GoscJPQLManager;
import controller.managedEntity.PokojJPQLManager;
import controller.managedEntity.RezerwacjaJPQLManager;
import model.Gosc;
import model.Pokoj;
import model.Rezerwacja;

import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;
import java.util.regex.Pattern;


@Stateful
@ManagedBean
@SessionScoped
public class SesjaBeans implements Serializable {
    private static final String LOGIN_SUCCESS = "zalogowany.xhtml?faces-redirect=true";
    private static final String LOGIN_FAILED = "niezalogowany.xhtml?faces-redirect=true";
    private static final String LOGIN_NOACIVATED = "";
    private static final String ACTIVATION_SUCCES = "index.xhtml?faces-redirect=true";
    private static final String ACTIVATION_ERROR = "";
    public SendMail mailer = new SendMail();

    EntityManager em = Connect.getConnect().createEntityManager();
    GoscJPQLManager goscjpql = new GoscJPQLManager();
    PokojJPQLManager pokojpql = new PokojJPQLManager();
    RezerwacjaJPQLManager rezerwacjajpql = new RezerwacjaJPQLManager();
    Platnosci plantosci = new Platnosci();
    Walidator walidator = new Walidator();


    private Gosc zalogowanyGosc = new Gosc();
    private boolean status;
    private boolean admin;

    public SesjaBeans() {

    }

    public String losowyKod() {
        GeneratorKodow msr = new GeneratorKodow();
        return msr.getLosowyKod();
    }


    public String aktywujKonto(LogowanieBeans logowanieBeans) {
        try {
            Gosc gosc = goscjpql.getKod(logowanieBeans.getKodAktywancyjny());
            if (!gosc.isAktywowane()) {
                gosc.setAktywowane(true);

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Poprawnie aktywowales konto.", ""));
                em.getTransaction().begin();
                em.merge(gosc);
                em.getTransaction().commit();
                em.clear();
            } else
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Konto jest już aktywne.", ""));
            return ACTIVATION_SUCCES;
        } catch (Exception ex) {
            return ACTIVATION_ERROR;
        }
    }

    private String utworzRezerwacje(Gosc gosc, RezerwacjaBeans rezerwacjaBeans) {
        Rezerwacja nowaRez = new Rezerwacja();
        nowaRez.setCzaspoczatkowy(rezerwacjaBeans.getCzaspoczatkowy());
        nowaRez.setCzaskoncowy(rezerwacjaBeans.getCzaskoncowy());
        nowaRez.setRozliczona(false);
        if (czyZajeteWTymCzasie(rezerwacjaBeans)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Data Niedostępna", "Wprowadz Poprawna"));
            return "";
        }
        Pokoj pok;
        try {
            pok = pokojpql.getPokoj(rezerwacjaBeans.getIdPokoju());
        } catch (Exception ex) {
            return "/NieMaPokoju.xhtml";
        }
        nowaRez.setPokoj(pok);
        nowaRez.setOsoba(gosc);
        em.getTransaction().begin();
        em.persist(nowaRez);
        em.getTransaction().commit();
        em.clear();
        plantosci.aktualizujRachunek(gosc);

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Udało Ci się stworzyć rezerwacje", "Milego Dnia"));
        return "";
    }

    public void zaplac() {
        plantosci.zaplacZaRezerwacje(getZalogowanyGosc());
        plantosci.aktualizujRachunek(getZalogowanyGosc());
    }


    public String zarezerwujZKontaZalogowanego(RezerwacjaBeans rezerwacjaBeans) {

        if (rezerwacjaBeans.getCzaspoczatkowy().getTime() > rezerwacjaBeans.getCzaskoncowy().getTime()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Niepoprawna data", "Czas poczatkowy jest pozniejszy niż końcowy"));
            return "";
        }

        if (!getStatus()) {
            return "GoscNieJestZalogowany!!";
        }
        Gosc gosc = getZalogowanyGosc();

        return utworzRezerwacje(gosc, rezerwacjaBeans);
    }

    private Gosc dodajGoscia(RezerwacjaBeans rezerwacjaBeans) {
        Gosc nowy = new Gosc();



        nowy.setImie(rezerwacjaBeans.getImie());
        nowy.setNazwisko(rezerwacjaBeans.getNazwisko());
        nowy.setWiek(rezerwacjaBeans.getWiek());
        nowy.setEmail(rezerwacjaBeans.getEmail());
        nowy.setPesel(rezerwacjaBeans.getPesel());
        nowy.setUsername(rezerwacjaBeans.getUsername());
        nowy.setPassword(rezerwacjaBeans.getPassword());
        nowy.setRachunek(0);
        nowy.setKod(losowyKod());


        em.getTransaction().begin();
        em.persist(nowy);
        em.getTransaction().commit();
        em.clear();
        return nowy;
    }

    public void aktualizujAdmina(Gosc gosc) {
        if (gosc.isAdmin())
            setAdmin(true);
    }

    public String zarezerwuj(RezerwacjaBeans rezerwacjaBeans) {


        if(!walidator.walidacjaGoscia(rezerwacjaBeans)){
            return "";
        }

        if (rezerwacjaBeans.getCzaspoczatkowy().getTime() > rezerwacjaBeans.getCzaskoncowy().getTime()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Niepoprawna data", "Czas poczatkowy jest pozniejszy niż końcowy"));
            return "";
        }

        if (czyIstniejejuztakiLogin(rezerwacjaBeans)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Podany Login Juz Istnieje", rezerwacjaBeans.getUsername()));
            return "";
        }
        if (czyIstniejejuztakiEmail(rezerwacjaBeans)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Podany Email Juz Istnieje", rezerwacjaBeans.getEmail()));
            return "";
        }

        Gosc gosc = dodajGoscia(rezerwacjaBeans);


        mailer.wyslij(gosc,rezerwacjaBeans);


        return utworzRezerwacje(gosc, rezerwacjaBeans);
    }

    public boolean czyIstniejejuztakiLogin(RezerwacjaBeans rezerwacjaBeans) {
        try {
            Gosc g = goscjpql.getGosc1(rezerwacjaBeans.getUsername());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String zaloguj(LogowanieBeans logowanieBeans) {
        try {
            Gosc gosc = goscjpql.getGosc(logowanieBeans.getLogin(), logowanieBeans.getHaslo());
            aktualizujAdmina(gosc);
            getZalogowanyGosc().setId(gosc.getId());
            getZalogowanyGosc().setNazwisko(gosc.getNazwisko());
            getZalogowanyGosc().setWiek(gosc.getWiek());
            getZalogowanyGosc().setEmail(gosc.getEmail());
            getZalogowanyGosc().setPesel(gosc.getPesel());
            getZalogowanyGosc().setImie(gosc.getImie());
            getZalogowanyGosc().setUsername(gosc.getUsername());
            getZalogowanyGosc().setPassword(gosc.getPassword());
            getZalogowanyGosc().setRachunek(gosc.getRachunek());
            getZalogowanyGosc().setRozliczony(gosc.isRozliczony());
            getZalogowanyGosc().setAktywowane(gosc.isAktywowane());


            if (!getZalogowanyGosc().isAktywowane()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Konto Nieaktywowane", "Aktywuj konto kodem aktywacyjnym"));
                return LOGIN_NOACIVATED;
            }
            setStatus(true);

            if (getZalogowanyGosc().isAdmin())
                setAdmin(true);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Witaj:", logowanieBeans.getLogin()));
            return LOGIN_SUCCESS;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Błąd Logowania: ", "Nieprawidłowe Dane"));
            return LOGIN_FAILED;
        }
    }

    public void wyloguj() {
        getZalogowanyGosc().setId(0);
        getZalogowanyGosc().setWiek(0);
        getZalogowanyGosc().setPesel("");
        getZalogowanyGosc().setImie("");
        getZalogowanyGosc().setNazwisko("");
        getZalogowanyGosc().setEmail("");
        getZalogowanyGosc().setUsername("");
        getZalogowanyGosc().setPassword("");
        getZalogowanyGosc().setRachunek(0);
        setStatus(false);
        setAdmin(false);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Zostałeś Wylogowany ", "Miłego Dnia"));
    }

    public String getWywietlRezerwacje() {

        try {
            List list = rezerwacjajpql.getListaRezerwacjiGoscia(zalogowanyGosc.getId());
            for (Object object : list) {
                System.out.println(((Rezerwacja) object).getId());
                System.out.println(((Rezerwacja) object).getOsoba().getId());

            }
            return "/ listarezerwacji.xhtml";

        } catch (Exception ex) {

            ex.printStackTrace();
            return "/nie udalo sie wyswietlic rezerwacji .xhtml";
        }


    }

    public void modyfikujGosc(RezerwacjaBeans rezerwacjaBeans) {
        Gosc nowy = new Gosc();
        nowy.setId(getZalogowanyGosc().getId());
        nowy.setImie(rezerwacjaBeans.getImie());
        nowy.setNazwisko(rezerwacjaBeans.getNazwisko());
        nowy.setWiek(rezerwacjaBeans.getWiek());
        nowy.setEmail(getZalogowanyGosc().getEmail());
        nowy.setPesel(rezerwacjaBeans.getPesel());
        nowy.setUsername(getZalogowanyGosc().getUsername());
        nowy.setPassword(rezerwacjaBeans.getPassword());
        nowy.setKod(getZalogowanyGosc().getKod());
        nowy.setRachunek(getZalogowanyGosc().getRachunek());
        nowy.setRozliczony(getZalogowanyGosc().isRozliczony());
        nowy.setAktywowane(getZalogowanyGosc().isAktywowane());
        em.getTransaction().begin();
        em.merge(nowy);
        em.getTransaction().commit();
        em.clear();

        rezerwacjaBeans.setImie(nowy.getImie());
        rezerwacjaBeans.setNazwisko(nowy.getNazwisko());
        rezerwacjaBeans.setWiek(nowy.getWiek());
        rezerwacjaBeans.setPesel(nowy.getPesel());
        rezerwacjaBeans.setPassword(nowy.getPassword());


    }

    public void otworzModfikacjeGoscia(RezerwacjaBeans rezerwacjaBeans) {
        rezerwacjaBeans.setImie(zalogowanyGosc.getImie());
        rezerwacjaBeans.setNazwisko(zalogowanyGosc.getNazwisko());
        rezerwacjaBeans.setWiek(zalogowanyGosc.getWiek());
        rezerwacjaBeans.setEmail(zalogowanyGosc.getEmail());
        rezerwacjaBeans.setPesel(zalogowanyGosc.getPesel());
        rezerwacjaBeans.setUsername(zalogowanyGosc.getUsername());
        rezerwacjaBeans.setPassword(zalogowanyGosc.getPassword());
    }

    public boolean czyZajeteWTymCzasie(RezerwacjaBeans rezerwacjaBeans) {
        List<Rezerwacja> listaNakladajacyhSie = rezerwacjajpql.getListaRezerwacjiZajetychWczasie(rezerwacjaBeans.getCzaspoczatkowy(), rezerwacjaBeans.getCzaskoncowy(), rezerwacjaBeans.getIdPokoju());

        if (listaNakladajacyhSie.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public boolean czyIstniejejuztakiEmail(RezerwacjaBeans rezerwacjaBeans) {
        try {
            Gosc g = goscjpql.getEmail(rezerwacjaBeans.getEmail());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String sprawdzDane(RezerwacjaBeans rezerwacjaBeans) {
        if(!walidator.walidacjaGoscia(rezerwacjaBeans)){
            return "";
        }

        if (rezerwacjaBeans.getCzaspoczatkowy().getTime() > rezerwacjaBeans.getCzaskoncowy().getTime()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Niepoprawna data", "Czas poczatkowy jest pozniejszy niż końcowy"));
            return "";
        }

        if (czyZajeteWTymCzasie(rezerwacjaBeans)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Data Niedostępna", "Wprowadz Poprawna"));
            return "";
        }
        if (czyIstniejejuztakiLogin(rezerwacjaBeans)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Podany Login Juz Istnieje", rezerwacjaBeans.getUsername()));
            return "";
        }

        if (czyIstniejejuztakiEmail(rezerwacjaBeans)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Podany Email Juz Istnieje", rezerwacjaBeans.getEmail()));
            return "";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Wprowadziles poprawne dane", ""));
        return "";
    }

    public String sprwadzDate(RezerwacjaBeans rezerwacjaBeans) {
        if(!walidator.walidacjaGoscia(rezerwacjaBeans)){
            return "";
        }

        if (rezerwacjaBeans.getCzaspoczatkowy().getTime() > rezerwacjaBeans.getCzaskoncowy().getTime()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Niepoprawna data", "Czas poczatkowy jest pozniejszy niż końcowy"));
            return "";
        }
        if (czyZajeteWTymCzasie(rezerwacjaBeans)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Data Niedostępna", "Wprowadz Poprawna"));
            return "";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Wprowadziles poprawne date", ""));
        return "";
    }

    public boolean zalogujGoogle(LogowanieBeans logowanieBeans) {
        try {
            Gosc gosc = goscjpql.getGoscGoogle(logowanieBeans.getEmail());
            aktualizujAdmina(gosc);
            getZalogowanyGosc().setId(gosc.getId());
            getZalogowanyGosc().setNazwisko(gosc.getNazwisko());
            getZalogowanyGosc().setWiek(gosc.getWiek());
            getZalogowanyGosc().setEmail(gosc.getEmail());
            getZalogowanyGosc().setPesel(gosc.getPesel());
            getZalogowanyGosc().setImie(gosc.getImie());
            getZalogowanyGosc().setUsername(gosc.getUsername());
            getZalogowanyGosc().setPassword(gosc.getPassword());
            getZalogowanyGosc().setRachunek(gosc.getRachunek());
            getZalogowanyGosc().setRozliczony(gosc.isRozliczony());
            getZalogowanyGosc().setAktywowane(gosc.isAktywowane());


            if (!getZalogowanyGosc().isAktywowane()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Konto nieaktywowane", "Aktywuj konto kodem aktywacyjnym"));
                return false;
            }
            setStatus(true);

            if (getZalogowanyGosc().isAdmin())
                setAdmin(true);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Witaj:", logowanieBeans.getLogin()));
            return true;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Błąd logowania: ", "Nie posiadamy w bazie użytkownika o podanym adresie email."));
            return false;
        }
    }

    public Gosc getZalogowanyGosc() {
        return zalogowanyGosc;
    }

    public void setZalogowanyGosc(Gosc zalogowanyGosc) {
        this.zalogowanyGosc = zalogowanyGosc;
    }

    public boolean getStatus() {
        return status; //do testu czy jest zalogowany
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

}


