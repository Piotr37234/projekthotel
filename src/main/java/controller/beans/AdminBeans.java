package controller.beans;


import controller.Connect;
import controller.managedEntity.GoscJPQLManager;
import controller.managedEntity.PokojJPQLManager;
import controller.managedEntity.RezerwacjaJPQLManager;
import model.Gosc;
import model.Pokoj;
import model.Rezerwacja;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import java.util.Date;


@ManagedBean
@RequestScoped
public class AdminBeans {
    EntityManager em = Connect.getConnect().createEntityManager();
    GoscJPQLManager goscjpql = new GoscJPQLManager();
    PokojJPQLManager pokojpql = new PokojJPQLManager();
    RezerwacjaJPQLManager rezerwacjajpql = new RezerwacjaJPQLManager();
    SesjaBeans sesjaBeans = new SesjaBeans();
    Walidator walidator = new Walidator();


    private Date czaspoczatkowy;
    private Date czaskoncowy;
    private double rachunek;
    private boolean rozliczony;
    private boolean admin;
    private String kod;
    private boolean aktywowane;
    private boolean rezerwacjaRozliczona;


    public void przygotujEdytujRezerwacje(RezerwacjaAdminBeans rezerwacjaAdminBeans) {
        Rezerwacja rezerwacja = null;
        Gosc gosc;
        Pokoj pokoj;

        try {
            rezerwacja = rezerwacjajpql.getRezerwacja(rezerwacjaAdminBeans.getIdRezerwacji());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Pomyślnie załadowano dane.", ""));
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Brak rezerwacji o podanym ID.", ""));
            rezerwacja = new Rezerwacja();
        }

        rezerwacjaAdminBeans.setCzaskoncowy(rezerwacja.getCzaskoncowy());
        rezerwacjaAdminBeans.setCzaspoczatkowy(rezerwacja.getCzaspoczatkowy());

        try {
            gosc = goscjpql.getGosc(rezerwacja.getOsoba().getId());
            rezerwacjaAdminBeans.setIdGoscia(rezerwacja.getOsoba().getId());
        } catch (Exception e) {
            return;
        }

        try {
            pokoj = pokojpql.getPokoj(rezerwacja.getPokoj().getId());
            rezerwacjaAdminBeans.setIdPokoju(rezerwacja.getPokoj().getId());
        } catch (Exception e) {
            return;
        }



    }

    public void przygotujEdytujGosc(RezerwacjaAdminBeans rezerwacjaAdminBeans) {
        Gosc gosc= null;

        try {
            gosc = goscjpql.getGosc(rezerwacjaAdminBeans.getIdGoscia());
            rezerwacjaAdminBeans.setKod(gosc.getKod());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Poymslnie zaladowano goscia.", ""));
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Brak goscia o podanym ID.", ""));
            gosc = new Gosc();
            rezerwacjaAdminBeans.setKod(sesjaBeans.losowyKod());
        }

        rezerwacjaAdminBeans.setImie(gosc.getImie());
        rezerwacjaAdminBeans.setWiek(gosc.getWiek());
        rezerwacjaAdminBeans.setPesel(gosc.getPesel());
        rezerwacjaAdminBeans.setNazwisko(gosc.getNazwisko());
        rezerwacjaAdminBeans.setEmail(gosc.getEmail());
        rezerwacjaAdminBeans.setUsername(gosc.getUsername());
        rezerwacjaAdminBeans.setPassword(gosc.getPassword());
        rezerwacjaAdminBeans.setRachunek(gosc.getRachunek());
        rezerwacjaAdminBeans.setRozliczony(gosc.isRozliczony());
        rezerwacjaAdminBeans.setAdmin(gosc.isAdmin());
        rezerwacjaAdminBeans.setAktywowane(gosc.isAktywowane());
    }

    public void dodajRezerwacje(RezerwacjaAdminBeans rezerwacjaAdminBeans) {

        Rezerwacja rezerwacja = new Rezerwacja();
        Gosc gosc;
        Pokoj pokoj;

        try {
            gosc = goscjpql.getGosc(rezerwacjaAdminBeans.getIdGoscia());
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Brak goscia o podanym ID.", ""));
            return;
        }


        try {
            pokoj = pokojpql.getPokoj(rezerwacjaAdminBeans.getIdPokoju());
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Brak pokoju o podanym ID.", ""));
            return;
        }


        if (!testPoprawnosciRezerwacji(rezerwacjaAdminBeans)) {
            return;
        }





        rezerwacja.setCzaskoncowy(rezerwacjaAdminBeans.getCzaskoncowy());
        rezerwacja.setCzaspoczatkowy(rezerwacjaAdminBeans.getCzaspoczatkowy());
        rezerwacja.setRozliczona(rezerwacjaAdminBeans.isRezerwacjaRozliczona());
        rezerwacja.setOsoba(gosc);
        rezerwacja.setPokoj(pokoj);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Pomyślnie dodano rezerwacje.", ""));
        em.getTransaction().begin();
        em.persist(rezerwacja);
        em.getTransaction().commit();
        em.clear();
    }
    public void zapiszEdytujRezerwacje(RezerwacjaAdminBeans rezerwacjaAdminBeans) {

        Rezerwacja rezerwacja = new Rezerwacja();
        Gosc gosc;
        Pokoj pokoj;

        if (!testPoprawnosciRezerwacji(rezerwacjaAdminBeans)) {
            return;
        }

        try {
            gosc = goscjpql.getGosc(rezerwacjaAdminBeans.getIdGoscia());
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Brak gościa o podanym ID.", ""));
            return;
        }

        try {
            pokoj = pokojpql.getPokoj(rezerwacjaAdminBeans.getIdPokoju());
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Brak pokoju o podanym ID.", ""));
            return;
        }

        rezerwacja.setId(rezerwacjaAdminBeans.getIdRezerwacji());
        rezerwacja.setCzaskoncowy(rezerwacjaAdminBeans.getCzaskoncowy());
        rezerwacja.setCzaspoczatkowy(rezerwacjaAdminBeans.getCzaspoczatkowy());
        rezerwacja.setRozliczona(rezerwacjaAdminBeans.isRezerwacjaRozliczona());
        rezerwacja.setOsoba(gosc);
        rezerwacja.setPokoj(pokoj);

        em.getTransaction().begin();
        em.merge(rezerwacja);
        em.getTransaction().commit();
        em.clear();
    }
    public void dodajGosc(RezerwacjaAdminBeans rezerwacjaAdminBeans) {
        Gosc nowy = new Gosc();

        if (!testPoprawnosciGoscia(rezerwacjaAdminBeans)) {
            return;
        }

        nowy.setRachunek(0);
        nowy.setAdmin(false);
        nowy.setKod(sesjaBeans.losowyKod());
        nowy.setAktywowane(true);

        nowy.setImie(rezerwacjaAdminBeans.getImie());
        nowy.setNazwisko(rezerwacjaAdminBeans.getNazwisko());
        nowy.setWiek(rezerwacjaAdminBeans.getWiek());
        nowy.setEmail(rezerwacjaAdminBeans.getEmail());
        nowy.setPesel(rezerwacjaAdminBeans.getPesel());
        nowy.setUsername(rezerwacjaAdminBeans.getUsername());
        nowy.setPassword(rezerwacjaAdminBeans.getPassword());
        nowy.setRachunek(rezerwacjaAdminBeans.getRachunek());
        nowy.setRozliczony(rezerwacjaAdminBeans.isRozliczony());
        nowy.setAdmin(rezerwacjaAdminBeans.isAdmin());
        nowy.setKod(rezerwacjaAdminBeans.getKod());
        nowy.setAktywowane(rezerwacjaAdminBeans.isAktywowane());
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Pomyślnie dodano goscią.", ""));
        em.getTransaction().begin();
        em.persist(nowy);
        em.getTransaction().commit();
        em.clear();
    }

    public boolean testPoprawnosciGoscia(RezerwacjaAdminBeans rezerwacjaAdminBeans) {
        if(!walidator.walidacjaGoscia(rezerwacjaAdminBeans)){
            return false;
        }

        if (sesjaBeans.czyIstniejejuztakiLogin(rezerwacjaAdminBeans)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Podany Login Juz Istnieje", rezerwacjaAdminBeans.getUsername()));
            return false;
        }
        if (sesjaBeans.czyIstniejejuztakiEmail(rezerwacjaAdminBeans)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Podany Email Juz Istnieje", rezerwacjaAdminBeans.getEmail()));
            return false;
        }
        return true;
    }
    public boolean testPoprawnosciGosciaEdycji(Gosc gosc, RezerwacjaAdminBeans rezerwacjaAdminBeans) {
        if(!walidator.walidacjaGoscia(rezerwacjaAdminBeans)){
            return false;
        }
        if (!(gosc.getUsername().equals(rezerwacjaAdminBeans.getUsername()))) {
            if (sesjaBeans.czyIstniejejuztakiLogin(rezerwacjaAdminBeans)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Podany Login Juz Istnieje", rezerwacjaAdminBeans.getUsername()));
                return false;
            }
        }
        if (!(gosc.getEmail().equals(rezerwacjaAdminBeans.getEmail()))) {
            if (sesjaBeans.czyIstniejejuztakiEmail(rezerwacjaAdminBeans)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Podany Email Juz Istnieje", rezerwacjaAdminBeans.getEmail()));
                return false;
            }
        }
        return true;
    }
    public boolean testPoprawnosciRezerwacji(RezerwacjaAdminBeans rezerwacjaAdminBeans) {
        if (rezerwacjaAdminBeans.getCzaspoczatkowy().getTime() > rezerwacjaAdminBeans.getCzaskoncowy().getTime()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Niepoprawna data", "Czas poczatkowy jest pozniejszy niż końcowy"));
            return false;
        }

        return true;
    }
    public void zapiszEdytujGosc(RezerwacjaAdminBeans rezerwacjaAdminBeans) {

        Gosc nowy = new Gosc();


        nowy.setId(rezerwacjaAdminBeans.getIdGoscia());
        nowy.setImie(rezerwacjaAdminBeans.getImie());
        nowy.setNazwisko(rezerwacjaAdminBeans.getNazwisko());
        nowy.setWiek(rezerwacjaAdminBeans.getWiek());
        nowy.setEmail(rezerwacjaAdminBeans.getEmail());
        nowy.setPesel(rezerwacjaAdminBeans.getPesel());
        nowy.setUsername(rezerwacjaAdminBeans.getUsername());
        nowy.setPassword(rezerwacjaAdminBeans.getPassword());
        nowy.setRachunek(rezerwacjaAdminBeans.getRachunek());
        nowy.setRozliczony(rezerwacjaAdminBeans.isRozliczony());
        nowy.setAdmin(rezerwacjaAdminBeans.isAdmin());
        nowy.setKod(rezerwacjaAdminBeans.getKod());
        nowy.setAktywowane(rezerwacjaAdminBeans.isAktywowane());

        if (!testPoprawnosciGosciaEdycji(nowy, rezerwacjaAdminBeans)) {
            return;
        }

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Pomyslnie zapisano zmiany.", ""));
        em.getTransaction().begin();
        em.merge(nowy);
        em.getTransaction().commit();
        em.clear();


    }
    public void dodajPokoj(PokojBeans pokojBeans) {
        Pokoj nowy = new Pokoj();
        nowy.setCena(pokojBeans.getCena());
        nowy.setIloscosob(pokojBeans.getIloscosob());
        nowy.setAdresobrazka(pokojBeans.getAdresobrazka());
        nowy.setNazwa(pokojBeans.getNazwa());
        nowy.setOpis_dlugi(pokojBeans.getOpis_dlugi());
        nowy.setOpis(pokojBeans.getOpis());
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Pomyslnie dodano pokoj.", ""));
        em.getTransaction().begin();
        em.persist(nowy);
        em.getTransaction().commit();
        em.clear();
    }
    public void przygotujEdytujPokoj(PokojBeans pokojBeans) {
        Pokoj pokoj = null;
        try {
            pokoj = pokojpql.getPokoj(pokojBeans.getId());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Pomyslnie zaladowano pokoj.", ""));
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Brak pokoju o podanym ID.", ""));
            pokoj = new Pokoj();
        }
        pokojBeans.setCena(pokoj.getCena());
        pokojBeans.setIloscosob(pokoj.getIloscosob());
        pokojBeans.setAdresobrazka(pokoj.getAdresobrazka());
        pokojBeans.setNazwa(pokoj.getNazwa());
        pokojBeans.setOpis_dlugi(pokoj.getOpis_dlugi());
        pokojBeans.setOpis(pokoj.getOpis());
    }
    public void zapiszEdytujPokoj(PokojBeans pokojBeans) {


        Pokoj nowy = new Pokoj();
        nowy.setId(pokojBeans.getId());
        nowy.setCena(pokojBeans.getCena());
        nowy.setIloscosob(pokojBeans.getIloscosob());
        nowy.setAdresobrazka(pokojBeans.getAdresobrazka());
        nowy.setNazwa(pokojBeans.getNazwa());
        nowy.setOpis_dlugi(pokojBeans.getOpis_dlugi());
        nowy.setOpis(pokojBeans.getOpis());
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Pomyslnie zapisano pokoj.", ""));
        em.getTransaction().begin();
        em.merge(nowy);
        em.getTransaction().commit();
        em.clear();
    }


}
