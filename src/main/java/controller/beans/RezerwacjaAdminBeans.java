package controller.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;


@ManagedBean
@RequestScoped
public class RezerwacjaAdminBeans extends RezerwacjaBeans implements Serializable {


    private int idGoscia;
    private int idRezerwacji;
    private int idPokoju;

    private Date czaspoczatkowy;
    private Date czaskoncowy;
    private String email;
    private String imie;
    private String nazwisko;
    private int wiek;
    private String pesel;
    private String username;
    private String password;
    private double rachunek;
    private boolean rozliczony;
    private boolean admin;
    private String kod;
    private boolean aktywowane;
    private boolean rezerwacjaRozliczona;


    public int getIdGoscia() {
        return idGoscia;
    }

    public void setIdGoscia(int idGoscia) {
        this.idGoscia = idGoscia;
    }


    public boolean isRezerwacjaRozliczona() {
        return rezerwacjaRozliczona;
    }

    public void setRezerwacjaRozliczona(boolean rezerwacjaRozliczona) {
        this.rezerwacjaRozliczona = rezerwacjaRozliczona;
    }

    public int getIdRezerwacji() {
        return idRezerwacji;
    }

    public void setIdRezerwacji(int idRezerwacji) {
        this.idRezerwacji = idRezerwacji;
    }



    public double getRachunek() {
        return rachunek;
    }

    public void setRachunek(double rachunek) {
        this.rachunek = rachunek;
    }

    public boolean isRozliczony() {
        return rozliczony;
    }

    public void setRozliczony(boolean rozliczony) {
        this.rozliczony = rozliczony;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public boolean isAktywowane() {
        return aktywowane;
    }

    public void setAktywowane(boolean aktywowane) {
        this.aktywowane = aktywowane;
    }




    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public int getIdPokoju() {
        return idPokoju;
    }

    public void setIdPokoju(int idPokoju) {
        this.idPokoju = idPokoju;
    }

    public Date getCzaspoczatkowy() {
        return czaspoczatkowy;
    }

    public void setCzaspoczatkowy(Date czaspoczatkowy) {
        this.czaspoczatkowy = czaspoczatkowy;
    }

    public Date getCzaskoncowy() {
        return czaskoncowy;
    }

    public void setCzaskoncowy(Date czaskoncowy) {
        this.czaskoncowy = czaskoncowy;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }
}
