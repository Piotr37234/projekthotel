package controller.beans;

import controller.Connect;
import controller.managedEntity.GoscJPQLManager;
import controller.managedEntity.PokojJPQLManager;
import model.Gosc;
import model.Pokoj;
import model.Rezerwacja;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@ManagedBean
@RequestScoped
public class PokojBeans implements Serializable {

    private int id;
    private String nazwa;
    private byte iloscosob;
    private String adresobrazka;
    private double cena;
    private String opis;
    private String opis_dlugi;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public byte getIloscosob() {
        return iloscosob;
    }

    public void setIloscosob(byte iloscosob) {
        this.iloscosob = iloscosob;
    }

    public String getAdresobrazka() {
        return adresobrazka;
    }

    public void setAdresobrazka(String adresobrazka) {
        this.adresobrazka = adresobrazka;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getOpis_dlugi() {
        return opis_dlugi;
    }

    public void setOpis_dlugi(String opis_dlugi) {
        this.opis_dlugi = opis_dlugi;
    }






}