package controller.beans;

import controller.Connect;
import controller.managedEntity.GoscJPQLManager;
import controller.managedEntity.PokojJPQLManager;
import model.Gosc;

import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Map;

@ManagedBean
@RequestScoped
public class LogowanieBeans implements Serializable {

    private String login;
    private String haslo;
    @ManagedProperty(value="#{param.kod}")
    private String kodAktywancyjny;
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKodAktywancyjny() {
        return kodAktywancyjny;
    }

    public void setKodAktywancyjny(String kodAktywancyjny) {
        this.kodAktywancyjny = kodAktywancyjny;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }
}
