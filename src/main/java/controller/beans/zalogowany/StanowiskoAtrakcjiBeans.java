package controller.beans.zalogowany;

import controller.Connect;
import model.StanowiskoAtrakcji;
import model.TypAtrakcji;

import javax.persistence.EntityManager;


public class StanowiskoAtrakcjiBeans {

    private String nazwa;
    private boolean dostepnosc;

    EntityManager em = Connect.getConnect().createEntityManager();

    public void dodajStanowiskoAtrakcji(TypAtrakcji typ) {
        StanowiskoAtrakcji stanowisko = new StanowiskoAtrakcji();
        stanowisko.setNazwa(nazwa);
        stanowisko.setDostepnosc(dostepnosc);
        stanowisko.setTyp(typ);
        em.getTransaction().begin();
        em.persist(stanowisko);
        em.getTransaction().commit();
    }




}
