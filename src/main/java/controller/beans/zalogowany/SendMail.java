package controller.beans.zalogowany;

import controller.beans.RezerwacjaBeans;
import controller.beans.SesjaBeans;
import model.Gosc;

import java.io.Serializable;
import java.util.Properties;

import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Named(value = "usuarioBean")
@SessionScoped

public class SendMail implements Serializable {

    private static final String HOST = "smtp.gmail.com";
    private static final int PORT = 465;
    // Adres email osby która wysyła maila
    private static final String FROM = "projekthotel123@gmail.com";
    // Hasło do konta osoby która wysyła maila
    private static final String PASSWORD = "korona1234";
    // Adres email osoby do której wysyłany jest mail
    private  String TO = "";
    // Temat wiadomości
    private static final String SUBJECT = "Potwierdzenie Rezerwacji";

    // Treść wiadomości
    private static final String CONTENT = "Informacje na temat rezerwacji:  \n" +
            "Czas rozpoczecia rezerwacji: 21.05.2018 \n"+
            "Czas końca rezerwacji: 23.05.2018 \n"+
            "Cena: 100zl";




    public void wyslij(Gosc gosc, RezerwacjaBeans rezerwacjaBeans) {
        try {
            new SendMail().send(gosc,rezerwacjaBeans);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public void send(Gosc gosc, RezerwacjaBeans rezerwacjaBeans) throws MessagingException {


        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtps");
        props.put("mail.smtps.auth", "true");

        // Inicjalizacja sesji
        Session mailSession = Session.getDefaultInstance(props);

        // ustawienie debagowania, jeśli nie chcesz oglądać logów to usuń
        // linijkę poniżej lub zmień wartość na false
        mailSession.setDebug(true);



        // Tworzenie wiadomości email erge
        MimeMessage message = new MimeMessage(mailSession);
        TO = rezerwacjaBeans.getEmail();
        message.setSubject(SUBJECT);
        message.setContent("Dziękujemy za rezerwacje:"+rezerwacjaBeans.getImie()+ " "+rezerwacjaBeans.getNazwisko()+ "\n" + "Rezerwacja na e-mail: " + rezerwacjaBeans.getEmail()+ "\n"+  "Czas rozpoczecia rezerwacji: "+ rezerwacjaBeans.getCzaspoczatkowy()
                + "\n" + "Czas końca rezerwacji: "+ rezerwacjaBeans.getCzaskoncowy()+ "\n" +"Cena:"+gosc.getRachunek()+" zl \n"  + "link aktywacyjny: http://localhost:8080/ARTEFAKT_XD/faces/resources/index.xhtml?kod="+gosc.getKod() + "\n" +"Życzymy miłego pobytu", "text/plain; charset=ISO-8859-2");
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(TO));

        Transport transport = mailSession.getTransport();
        transport.connect(HOST, PORT, FROM, PASSWORD);

        // wysłanie wiadomości
        transport.sendMessage(message, message
                .getRecipients(Message.RecipientType.TO));
        transport.close();
    }











}
