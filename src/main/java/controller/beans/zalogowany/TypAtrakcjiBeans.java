package controller.beans.zalogowany;

import controller.Connect;
import model.StanowiskoAtrakcji;
import model.TypAtrakcji;

import javax.persistence.EntityManager;


public class TypAtrakcjiBeans {

    private int id;
    private String cena;
    private String opis;


    EntityManager em = Connect.getConnect().createEntityManager();


    public void dodajTypAtrakcji() {
        TypAtrakcji typ = new TypAtrakcji();
        typ.setCena(cena);
        typ.setOpis(opis);

        em.getTransaction().begin();
        em.persist(typ);
        em.getTransaction().commit();
    }




}
