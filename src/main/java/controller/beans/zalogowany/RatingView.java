package controller.beans.zalogowany;

import controller.Connect;
import controller.beans.PokojBeans;
import controller.managedEntity.PokojJPQLManager;
import model.Pokoj;

import javax.ejb.Stateful;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import java.io.Serializable;


@Stateful
@ManagedBean
@SessionScoped
public class RatingView implements Serializable {

    public   String lokalizacja;
    public   String obsluga;
    public   String dostepnosc;
    public   String czystosc;
    public double suma2;
    int przezile=1;

    PokojJPQLManager pokojpql = new PokojJPQLManager();
    EntityManager em = Connect.getConnect().createEntityManager();



    public void suma(Pokoj pokoj, int przezile){

        suma2= (((Integer.parseInt(lokalizacja)+ Integer.parseInt(obsluga)+ Integer.parseInt(dostepnosc)+ Integer.parseInt(czystosc))/4.0)/przezile);

          pokoj = pokojpql.getPokoj(pokoj.getId());
          pokoj.setSredniapokoju(suma2);
          double zmienna = pokoj.getSredniapokoju();

        przezile++;

//        pokojj.setSredniapokoju(suma2);
//
//        em.getTransaction().begin();
//        em.merge(pokojj);
//        em.getTransaction().commit();
//        em.clear();
    }

    public void dodajelo(PokojBeans pokelo){

     Pokoj nowy = new Pokoj();
       nowy.setCena(pokelo.getCena());
       nowy.setIloscosob(pokelo.getIloscosob());
          nowy.setAdresobrazka(pokelo.getAdresobrazka());
         nowy.setNazwa(pokelo.getNazwa());
        nowy.setOpis_dlugi(pokelo.getOpis_dlugi());
        nowy.setOpis(pokelo.getOpis());
       nowy.setSredniapokoju(suma2);


        em.getTransaction().begin();
       em.persist(nowy);
       em.getTransaction().commit();

    }






    public double getSuma2() {
        return suma2;
    }

    public void setSuma2(double suma2) {
        this.suma2 = suma2;
    }

    public String getLokalizacja() {
        return lokalizacja;
    }

    public void setLokalizacja(String lokalizacja) {
        this.lokalizacja = lokalizacja;
    }

    public String getObsluga() {
        return obsluga;
    }

    public void setObsluga(String obsluga) {
        this.obsluga = obsluga;
    }

    public String getDostepnosc() {
        return dostepnosc;
    }

    public void setDostepnosc(String dostepnosc) {
        this.dostepnosc = dostepnosc;
    }

    public String getCzystosc() {
        return czystosc;
    }

    public void setCzystosc(String czystosc) {
        this.czystosc = czystosc;
    }
}



