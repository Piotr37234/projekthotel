package controller.beans;


import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.regex.Pattern;


public class Walidator {

    String email = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    String imie = "^[a-zA-Z]{2,50}$";
    String nazwisko = "^[a-zA-Z]{2,50}$";
    String pesel = "^[-+]?\\d+$";
    String username = "^[a-zA-Z0-9_-]{5,15}$";
    String password = "^[a-zA-Z0-9]{5,15}$";

    Pattern pattern_email = Pattern.compile(email);
    Pattern pattern_imie = Pattern.compile(imie);
    Pattern pattern_nazwisko = Pattern.compile(nazwisko);
    Pattern pattern_pesel = Pattern.compile(pesel);
    Pattern pattern_username = Pattern.compile(username);
    Pattern pattern_password = Pattern.compile(password);


    public boolean walidacjaGoscia(RezerwacjaBeans rezerwacjaBeans){
        if(!pattern_email.matcher(rezerwacjaBeans.getEmail()).matches()){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Blad Niepoprawny E-mail", "Niepoprawny E-mail"));
            return false;//zly email;
        }else if(!pattern_imie.matcher(rezerwacjaBeans.getImie()).matches()){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Blad Niepoprawne Imie", "Niepoprawne Imie"));
            return false;//zle imie;
        }else if(!pattern_nazwisko.matcher(rezerwacjaBeans.getNazwisko()).matches()){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Blad Niepoprawne Nazwisko", "Niepoprawne Nazwisko"));
            return false;//zle nazwisko;
        }else if(!pattern_pesel.matcher(rezerwacjaBeans.getPesel()).matches() || rezerwacjaBeans.getPesel().length()!=11){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Blad Niepoprawny Pesel", "Niepoprawny Pesel"));
            return false;//zly pesel;
        }else if(!pattern_username.matcher(rezerwacjaBeans.getUsername()).matches()){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Blad Niepoprawna Username", "Niepoprawna Username"));
            return false;//zle username;
        }else if(!pattern_password.matcher(rezerwacjaBeans.getPassword()).matches()){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Blad Niepoprawny Password", "Niepoprawny Password"));
            return false;//zle haslo;
        }else if(rezerwacjaBeans.getWiek()<16 || rezerwacjaBeans.getWiek()>120){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Blad Wiek należy podać z przedzialu od 16 do 120", "Wiek należy podać z przedzialu od 16 do 120"));
            return false;//zly wiek;
        }
        return true;
    }



}
