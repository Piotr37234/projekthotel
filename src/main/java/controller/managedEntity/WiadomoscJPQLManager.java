package controller.managedEntity;

import controller.Connect;
import model.Wiadomosc;

import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;


@Named
@RequestScoped
public class WiadomoscJPQLManager implements Serializable {


    public WiadomoscJPQLManager(){


    }
    public List getListaWiadomosci(){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("Wiadomosc.findAll")
                .getResultList();
        return  list;
    }


    public Wiadomosc getWiadomosc(int szukaneId){

        Wiadomosc wynik = (Wiadomosc)Connect.getConnect().createEntityManager().createNamedQuery("Wiadomosc.findbyId")
                .setParameter("id",szukaneId)
                .getSingleResult();
        return wynik;
    }


    public List getListaWiadomosciNadanychPrzezGosciaOID(int szukaneId){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("Wiadomosc.nadawca")
                .setParameter("idnadawcy",szukaneId)
                .getResultList();

        return list;
    }

    public List getListaWiadomosciOdebranychPrzezGosciaOID(int szukaneId){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("Wiadomosc.odbiorca")
                .setParameter("idodbiorcy",szukaneId)
                .getResultList();

        return list;
    }



}