package controller.managedEntity;

import controller.Connect;
import model.Rezerwacja;
import model.StanowiskoAtrakcji;

import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Named
@RequestScoped
public class StanowiskoAtrakcjiJPQLManager implements Serializable {


    public StanowiskoAtrakcjiJPQLManager(){


    }
    public List<StanowiskoAtrakcji> getListaStanowisk(){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("StanowiskoAtrakcji.findAll")
                .getResultList();
        return  list;
    }


    public StanowiskoAtrakcji getStanowisko(int szukaneId){

        StanowiskoAtrakcji wynik = (StanowiskoAtrakcji)Connect.getConnect().createEntityManager().createNamedQuery("StanowiskoAtrakcji.findbyId")
                .setParameter("id",szukaneId)
                .getSingleResult();
        return wynik;
    }

    public List<StanowiskoAtrakcji> getStanowiskaTypuOID(int szukaneId){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("StanowiskoAtrakcji.typ")
                .setParameter("id",szukaneId)
                .getResultList();
        return list;
    }

    public List<StanowiskoAtrakcji> getStanowiskaDostepneTypuOID(int szukaneId){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("StanowiskoAtrakcji.typ.dostepne")
                .setParameter("id",szukaneId)
                .getResultList();
        return list;
    }

}
