package controller.managedEntity;

import controller.Connect;
import model.Pracownik;
import model.RezerwacjaStanowiskaAtrakcji;

import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;


@Named
@RequestScoped
public class RezerwacjaStanowiskaAtrakcjiPQLManager implements Serializable {


    public RezerwacjaStanowiskaAtrakcjiPQLManager(){


    }
    public List<RezerwacjaStanowiskaAtrakcji> getListaRezerwacjiStanowiksAtrakcji(){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("RezerwacjaStanowiskaAtrakcji.findAll")
                .getResultList();
        return  list;
    }


    public RezerwacjaStanowiskaAtrakcji getRezerwacjaAtrakcji(int szukaneId){

        RezerwacjaStanowiskaAtrakcji wynik = (RezerwacjaStanowiskaAtrakcji)Connect.getConnect().createEntityManager().createNamedQuery("RezerwacjaStanowiskaAtrakcji.findbyId")
                .setParameter("id",szukaneId)
                .getSingleResult();
        return wynik;
    }


    public List<RezerwacjaStanowiskaAtrakcji> getListaRezerwacjiAtrakcjiGosciaOID(int szukaneId){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("RezerwacjaStanowiskaAtrakcji.findRezerwacjaGoscia")
                .setParameter("idgosc",szukaneId)
                .getResultList();

        return list;
    }

    public List<RezerwacjaStanowiskaAtrakcji> getListaRezerwacjiAtrakcjiStanowiskaOID(int szukaneId){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("RezerwacjaStanowiskaAtrakcji.findStanowisko")
                .setParameter("idstanowiska",szukaneId)
                .getResultList();

        return list;
    }


}
