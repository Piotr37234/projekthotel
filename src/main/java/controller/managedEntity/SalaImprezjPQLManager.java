package controller.managedEntity;

import controller.Connect;
import model.SalaImprez;

import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;


@Named
@RequestScoped
public class SalaImprezjPQLManager implements Serializable {


    public SalaImprezjPQLManager(){


    }
    public List getListaSalImprez(){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("SalaImprez.findAll")
                .getResultList();
        return  list;
    }


    public SalaImprez getSalaImprez(int szukaneId){

        SalaImprez wynik = (SalaImprez)Connect.getConnect().createEntityManager().createNamedQuery("SalaImprez.findbyId")
                .setParameter("id",szukaneId)
                .getSingleResult();
        return wynik;
    }





}
