package controller.managedEntity;

import controller.Connect;
import model.TypAtrakcji;

import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;


@Named
@RequestScoped
public class TypAtrakcjiJPQLManager implements Serializable {


    public TypAtrakcjiJPQLManager(){


    }
    public List<TypAtrakcji> getListaTypowAtrakcji(){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("TypObiektuAtrakcji.findAll")
                .getResultList();
        return  list;
    }


    public TypAtrakcjiJPQLManager getTypAtrakcji(int szukaneId){

        TypAtrakcjiJPQLManager wynik = (TypAtrakcjiJPQLManager)Connect.getConnect().createEntityManager().createNamedQuery("TypObiektuAtrakcji.findbyId")
                .setParameter("id",szukaneId)
                .getSingleResult();
        return wynik;
    }



}
