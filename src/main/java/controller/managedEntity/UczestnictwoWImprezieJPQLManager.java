package controller.managedEntity;

import controller.Connect;
import model.RezerwacjaStanowiskaAtrakcji;
import model.UczestnictwoWImprezie;

import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;


@Named
@RequestScoped
public class UczestnictwoWImprezieJPQLManager implements Serializable {


    public UczestnictwoWImprezieJPQLManager(){


    }
    public List getListaUczestnicwWImprezie(){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("UczestnictwoWImprezie.findAll")
                .getResultList();
        return  list;
    }


    public UczestnictwoWImprezie getUczestnictwoWImprezie(int szukaneId){

        UczestnictwoWImprezie wynik = (UczestnictwoWImprezie)Connect.getConnect().createEntityManager().createNamedQuery("UczestnictwoWImprezie.findbyId")
                .setParameter("id",szukaneId)
                .getSingleResult();
        return wynik;
    }


    public List getListaUczestnictWImprezachGosciaOID(int szukaneId){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("UczestnictwoWImprezie.FindRezerwacjeGoscia")
                .setParameter("idgosc",szukaneId)
                .getResultList();

        return list;
    }

    public List getListaUczestnictWImprezieOID(int szukaneId){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("UczestnictwoWImprezie.FindImprezy")
                .setParameter("idimprezy",szukaneId)
                .getResultList();

        return list;
    }


}