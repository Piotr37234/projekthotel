package controller.managedEntity;


import controller.Connect;
import model.Pokoj;

import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.io.Serializable;
import java.util.List;

@Named
@RequestScoped
public class PokojJPQLManager implements Serializable {


    public PokojJPQLManager() {

    }


    public List getListaPokoi() {
        List list = Connect.getConnect().createEntityManager().createNamedQuery("Pokoj.findAll").getResultList();
        return list;
    }


    public Pokoj getPokoj(int szukaneId) {

        Pokoj wynik = (Pokoj) Connect.getConnect().createEntityManager().createNamedQuery("Pokoj.findbyId")
                .setParameter("id", szukaneId)
                .getSingleResult();

        return wynik;
    }


}
