package controller.managedEntity;

import controller.Connect;
import model.Impreza;
import model.Rezerwacja;

import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;


@Named
@RequestScoped
public class ImprezaJPQLManager implements Serializable {


    public ImprezaJPQLManager(){


    }
    public List getListaImprez(){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("Impreza.findAll")
                .getResultList();
        return  list;
    }


    public Impreza getImpreza(int szukaneId){

        Impreza wynik = (Impreza)Connect.getConnect().createEntityManager().createNamedQuery("Impreza.findbyId")
                .setParameter("id",szukaneId)
                .getSingleResult();
        return wynik;
    }





}
