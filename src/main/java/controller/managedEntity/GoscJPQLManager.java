package controller.managedEntity;

import controller.Connect;
import model.Gosc;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;


@ManagedBean
@RequestScoped
public class GoscJPQLManager implements Serializable {


    public GoscJPQLManager() {

    }

    public List<Gosc> getListaGosci() {
        List<Gosc> list = Connect.getConnect().createEntityManager().createNamedQuery("Gosc.findAll").getResultList();
        return list;
    }

    public int test() {
        return 2;
    }

    public Gosc getGosc(int szukaneId) {
        Gosc wynik = (Gosc) Connect.getConnect().createEntityManager().createNamedQuery("Gosc.findbyId")
                .setParameter("id", szukaneId)
                .getSingleResult();
        return wynik;
    }


    public Gosc getGosc(String logingoscia, String haslogoscia) { // zwaraca tylko jeden albo nic

        Gosc wynikzapytania = (Gosc) Connect.getConnect().createEntityManager().createNamedQuery("Gosc.findPeselGosc")
                .setParameter("username", logingoscia)
                .setParameter("password", haslogoscia)
                .getSingleResult();

        return wynikzapytania;
    }
    public Gosc getGoscGoogle(String email) { // zwaraca tylko jeden albo nic

        Gosc wynikzapytania = (Gosc) Connect.getConnect().createEntityManager().createNamedQuery("Gosc.findbyEmail")
                .setParameter("email", email)
                .getSingleResult();

        return wynikzapytania;
    }
    public Gosc getEmail(String email) { // zwaraca tylko jeden albo nic

        Gosc wynikzapytania = (Gosc) Connect.getConnect().createEntityManager().createNamedQuery("Gosc.findbyEmail")
                .setParameter("email", email)
                .getSingleResult();

        return wynikzapytania;
    }

    public Gosc getGosc1(String logingoscia) {
        Gosc wynik = (Gosc) Connect.getConnect().createEntityManager().createNamedQuery("Gosc.findLogin")
                .setParameter("username", logingoscia)
                .getSingleResult();
        return wynik;
    }
    public Gosc getKod(String kod) {
        Gosc wynik = (Gosc) Connect.getConnect().createEntityManager().createNamedQuery("Gosc.findbyKod")
                .setParameter("kod", kod)
                .getSingleResult();
        return wynik;
    }


    public Double getZaplataZaRezerwacje(int idgoscia) {

        List list = Connect.getConnect().createEntityManager().createNamedQuery("Gosc.rachunekZaRezerwacje")
                .setParameter("idgosc", idgoscia)
                .getResultList();

        if (list.get(0) == null) {
            return 0.0;
        } else {
            return (Double) list.get(0);
        }
    }


}