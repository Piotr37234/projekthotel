package controller.managedEntity;

import controller.Connect;
import model.Usluga;

import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;


@Named
@RequestScoped
public class UslugaJPQLManager implements Serializable {


    public UslugaJPQLManager(){


    }
    public List getListaUslug(){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("Usluga.findAll")
                .getResultList();
        return  list;
    }


    public Usluga getUsluga(int szukaneId){

        Usluga wynik = (Usluga)Connect.getConnect().createEntityManager().createNamedQuery("Usluga.findbyId")
                .setParameter("id",szukaneId)
                .getSingleResult();
        return wynik;
    }


    public List getListaUslugPracownikaOID(int szukaneId){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("Usluga.FindPracownik")
                .setParameter("idpracownika",szukaneId)
                .getResultList();

        return list;
    }




}