package controller.managedEntity;

import controller.Connect;
import model.Rezerwacja;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@ManagedBean
@RequestScoped
public class RezerwacjaJPQLManager implements Serializable {


    public RezerwacjaJPQLManager(){


    }
    public List<Rezerwacja> getListaRezerwacjiGoscia(int idSzukane){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("Rezerwacja.FindRezerwacjeGoscia")
                .setParameter("idgosc",idSzukane)
                .getResultList();
        return  list;
    }


    public List getListaRezerwacji(){
        List list = Connect.getConnect().createEntityManager().createNamedQuery("Rezerwacja.findAll").getResultList();
        return list;
    }

    public Rezerwacja getRezerwacja(int szukaneId){

        Rezerwacja wynik = (Rezerwacja)Connect.getConnect().createEntityManager().createNamedQuery("Rezerwacja.findbyId")
                .setParameter("id",szukaneId)
                .getSingleResult();
        return wynik;
    }


    public List<Rezerwacja> getListaRezerwacjiZajetychWczasie(Date czaspoczatkowy, Date czaskoncowy,int id_pokoju){
        List list = Connect.getConnect().createEntityManager().createNamedQuery("Rezerwacja.zajeteWczasie")
                .setParameter("czaspoczatkowy",czaspoczatkowy)
                .setParameter("czaskoncowy",czaskoncowy)
                .setParameter("id_pokoju",id_pokoju)
                .getResultList();
        return  list;
    }



}
