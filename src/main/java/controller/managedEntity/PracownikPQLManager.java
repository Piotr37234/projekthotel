package controller.managedEntity;

import controller.Connect;
import model.Pracownik;


import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;


@Named
@RequestScoped
public class PracownikPQLManager implements Serializable {


    public PracownikPQLManager(){


    }
    public List getListaPracownikow(int idSzukane){

        List list = Connect.getConnect().createEntityManager().createNamedQuery("Pracownik.findAll")
                .getResultList();
        return  list;
    }


    public Pracownik getPracownik(int szukaneId){

        Pracownik wynik = (Pracownik)Connect.getConnect().createEntityManager().createNamedQuery("Pracownik.findbyId")
                .setParameter("id",szukaneId)
                .getSingleResult();
        return wynik;
    }



}
