package test;

import model.Pokoj;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;

public class TestBazyDanych {

//w tej paczce sa testy czyli rzeczy do zabawy :)
//to umozliwa sprawdzenie bazy danych bez serwera

    public static void main(String args[]) {


        Pokoj pok1 = new Pokoj();
        pok1.setIloscosob((byte)2);
        pok1.setNazwa("Pokój 3 osobowy");
        pok1.setOpis("bardzo dobry opis pokoju");
        pok1.setAdresobrazka("3osPok.jpg");
        pok1.setCena(200.0);
        pok1.setOpis_dlugi("Bardzo dlugi napis poko1");


        Pokoj pok2 = new Pokoj();
        pok2.setNazwa("Pokój 3 osobowy");
        pok2.setOpis("bardzo dobry opis pokoju");
        pok2.setIloscosob((byte) 3);
        pok2.setAdresobrazka("3osPok.jpg");
        pok2.setCena(400.0);
        pok2.setOpis_dlugi("Bardzo dlugi napis pokoj2");


        Pokoj pok3 = new Pokoj();
        pok3.setNazwa("Pokój 3 osobowy");
        pok3.setOpis("bardzo dobry opis pokoju");
        pok3.setIloscosob((byte) 3);
        pok3.setAdresobrazka("3osPok.jpg");
        pok3.setCena(350.0);
        pok3.setOpis_dlugi("Bardzo dlugi napis poko3");



        try {

            EntityManagerFactory k = Persistence.createEntityManagerFactory("NewPersistenceUnit");
            EntityManager em = k.createEntityManager();

            em.getTransaction().begin();
            em.persist(pok1);
            em.persist(pok2);
            em.persist(pok3);
            em.getTransaction().commit();

            ArrayList lista = (ArrayList) em.createNamedQuery("Pokoj.findAll").getResultList();


            for (Object l : lista) {
                System.out.println("******");
                System.out.println(((Pokoj) l).getId());
                System.out.println(((Pokoj) l).getNazwa());
                System.out.println("******");
            }


        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Message: " + e.getMessage());
            System.out.println("LokalizedMessage: " + e.getLocalizedMessage());
            System.out.println("Cause: " + e.getCause());


        }

    }


}
