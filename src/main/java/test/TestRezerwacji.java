package test;

import model.Gosc;
import model.Pokoj;
import model.Rezerwacja;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TestRezerwacji {


    public static void main(String args[]) {

        Pokoj p = new Pokoj();
        p.setNazwa("rozny pokoj");
        p.setIloscosob((byte) 1);
        p.setOpis("Tu kieszka przemo");
        p.setAdresobrazka("elo.jpg");

//        Gosc g = new Gosc();
//        g.setImie("Marian");
//        g.setNazwisko("Janowski");
//        g.setWiek(34);
//
//


        EntityManagerFactory k = Persistence.createEntityManagerFactory("NewPersistenceUnit");
        EntityManager em = k.createEntityManager();

        em.getTransaction().begin();
       // em.persist(g);
        em.persist(p);
        em.getTransaction().commit();

     //   System.out.println(p.getId());


        // em.close();
//        Rezerwacja rez = new Rezerwacja();
//        rez.setCzaspoczatkowy(Date.valueOf(LocalDate.parse("2011-03-03")));
//        rez.setCzaskoncowy(Date.valueOf(LocalDate.parse("2011-04-03")));
//        rez.setOsoba(g);
//        rez.setPokoj(p);
//
//
//        em.getTransaction().begin();
//        em.persist(rez);
//        em.getTransaction().commit();


    }

}
