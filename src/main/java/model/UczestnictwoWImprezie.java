package model;

import javax.persistence.*;


@NamedQueries({
        @NamedQuery(name = "UczestnictwoWImprezie.findAll", query = "SELECT p FROM UczestnictwoWImprezie p"),
        @NamedQuery(name = "UczestnictwoWImprezie.findbyId", query = "SELECT p FROM UczestnictwoWImprezie p WHERE p.id=:id"),
        @NamedQuery(name = "UczestnictwoWImprezie.FindRezerwacjeGoscia", query = "SELECT p FROM UczestnictwoWImprezie p WHERE p.gosc.id=:idgosc"),
        @NamedQuery(name = "UczestnictwoWImprezie.FindImprezy", query = "SELECT p FROM UczestnictwoWImprezie p WHERE p.impreza.id=:idimprezy")
}
)



@Entity
public class UczestnictwoWImprezie {

    @Id
    @GeneratedValue(generator="UczestnictwoWImprezie_seq")
    @SequenceGenerator(name="UczestnictwoWImprezie_seq",sequenceName="UczestnictwoWImprezie_seq", allocationSize=1)

    private int id;

    @ManyToOne
    private Gosc gosc;
    @ManyToOne
    private Impreza impreza;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Gosc getGosc() {
        return gosc;
    }

    public void setGosc(Gosc osoba) {
        this.gosc = osoba;
    }

    public Impreza getImpreza() {
        return impreza;
    }

    public void setImpreza(Impreza impreza) {
        this.impreza = impreza;
    }
}
