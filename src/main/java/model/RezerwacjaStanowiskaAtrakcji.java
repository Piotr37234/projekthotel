package model;

import javax.persistence.*;


@NamedQueries({
        @NamedQuery(name = "RezerwacjaStanowiskaAtrakcji.findAll", query = "SELECT p FROM RezerwacjaStanowiskaAtrakcji p"),
        @NamedQuery(name = "RezerwacjaStanowiskaAtrakcji.findbyId", query = "SELECT p FROM RezerwacjaStanowiskaAtrakcji p WHERE p.id=:id"),
        @NamedQuery(name = "RezerwacjaStanowiskaAtrakcji.findRezerwacjaGoscia", query = "SELECT p FROM RezerwacjaStanowiskaAtrakcji p WHERE p.gosc.id=:idgosc"),
        @NamedQuery(name = "RezerwacjaStanowiskaAtrakcji.findStanowisko", query = "SELECT p FROM RezerwacjaStanowiskaAtrakcji p WHERE p.stanowiskoAtrakcji.id=:idstanowiska")
}
)



@Entity
public class RezerwacjaStanowiskaAtrakcji {

    @Id
    @GeneratedValue(generator="RezerwacjaStanowiskaAtrakcji_seq")
    @SequenceGenerator(name="RezerwacjaStanowiskaAtrakcji_seq",sequenceName="RezerwacjaStanowiskaAtrakcji_seq", allocationSize=1)

    private int id;

    @ManyToOne
    private Gosc gosc;
    @ManyToOne
    private StanowiskoAtrakcji stanowiskoAtrakcji;

    public Gosc getGosc() {
        return gosc;
    }

    public void setGosc(Gosc gosc) {
        this.gosc = gosc;
    }

    public StanowiskoAtrakcji getStanowiskoAtrakcji() {
        return stanowiskoAtrakcji;
    }

    public void setStanowiskoAtrakcji(StanowiskoAtrakcji stanowiskoAtrakcji) {
        this.stanowiskoAtrakcji = stanowiskoAtrakcji;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }




}
