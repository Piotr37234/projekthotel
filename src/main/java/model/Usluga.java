package model;

import javax.persistence.*;
import java.util.Objects;


@NamedQueries({
        @NamedQuery(name = "Usluga.findAll", query = "SELECT p FROM Usluga p"),
        @NamedQuery(name = "Usluga.findbyId", query = "SELECT p FROM Usluga p WHERE p.id=:id"),
        @NamedQuery(name = "Usluga.FindPracownik", query = "SELECT p FROM Usluga p WHERE p.pracownik.id=:idpracownika"),

}
)


@Entity
public class Usluga {




    @Id
    @GeneratedValue(generator="Usluga_seq")
    @SequenceGenerator(name="Usluga_seq",sequenceName="Usluga_seq", allocationSize=1)
    private int id;
    private String nazwa;
    private int cena;
    private String opis;
    @ManyToOne
    private Pracownik pracownik;




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Pracownik getPracownik() {
        return pracownik;
    }

    public void setPracownik(Pracownik pracownik) {
        this.pracownik = pracownik;
    }







}
