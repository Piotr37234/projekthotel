package model;

import javax.persistence.*;
import java.util.Objects;



@NamedQueries({

        @NamedQuery(name = "Pokoj.findAll", query = "SELECT p FROM Pokoj p"),
        @NamedQuery(name = "Pokoj.findbyId", query = "SELECT p FROM Pokoj p WHERE p.id=:id")

}
)

@Entity
public class Pokoj {

    @Id
    @GeneratedValue(generator="Pokoj_seq")
    @SequenceGenerator(name="Pokoj_seq",sequenceName="Pokoj_seq", allocationSize=1)

    private int id;
    private String nazwa;
    private byte iloscosob;
    private String adresobrazka;
    private double cena;
    private String opis;
    private String opis_dlugi;
    private double sredniapokoju;

    public double getSredniapokoju() {
        return sredniapokoju;
    }

    public void setSredniapokoju(double sredniapokoju) {
        this.sredniapokoju = sredniapokoju;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public byte getIloscosob() {
        return iloscosob;
    }
    public void setIloscosob(byte iloscosob) {
        this.iloscosob = iloscosob;
    }
    public String getNazwa() {
        return nazwa;
    }
    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
    public String getOpis() {
        return opis;
    }
    public void setOpis(String opis) {
        this.opis = opis;
    }
    public double getCena() {
        return cena;
    }
    public void setCena(double cena) {
        this.cena = cena;
    }
    public String getOpis_dlugi() { return opis_dlugi; }
    public void setOpis_dlugi(String opis_dlugi) { this.opis_dlugi = opis_dlugi; }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pokoj pokoj = (Pokoj) o;
        return id == pokoj.id &&
                iloscosob == pokoj.iloscosob &&
                Objects.equals(nazwa, pokoj.nazwa) &&
                Objects.equals(opis, pokoj.opis);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, nazwa, iloscosob, opis);
    }


    public String getAdresobrazka() {
        return adresobrazka;
    }

    public void setAdresobrazka(String adresobrazka) {
        this.adresobrazka = adresobrazka;
    }
}
