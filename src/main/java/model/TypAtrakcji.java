package model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;


@NamedQueries({
        @NamedQuery(name = "TypObiektuAtrakcji.findAll", query = "SELECT p FROM TypAtrakcji p"),
        @NamedQuery(name = "TypObiektuAtrakcji.findbyId", query = "SELECT p FROM TypAtrakcji p WHERE p.id=:id"),

}
)


@Entity
public class TypAtrakcji {

    @Id
    @GeneratedValue(generator="TypObiektuAtrakcji_seq")
    @SequenceGenerator(name="TypObiektuAtrakcji_seq",sequenceName="TypObiektuAtrakcji_seq", allocationSize=1)
    private int id;
    private String cena;
    private String opis;






    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCena() {
        return cena;
    }

    public void setCena(String cena) {
        this.cena = cena;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }







}
