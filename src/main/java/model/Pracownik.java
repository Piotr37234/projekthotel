package model;

import javax.persistence.*;
import java.util.Objects;


@NamedQueries({
        @NamedQuery(name = "Pracownik.findAll", query = "SELECT p FROM Pracownik p"),
        @NamedQuery(name = "Pracownik.findbyId", query = "SELECT p FROM Pracownik p WHERE p.id=:id"),

}
)


@Entity
public class Pracownik {




    @Id
    @GeneratedValue(generator="Pracownik_seq")
    @SequenceGenerator(name="Pracownik_seq",sequenceName="Pracownik_seq", allocationSize=1)
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStanowisko() {
        return stanowisko;
    }

    public void setStanowisko(String stanowisko) {
        this.stanowisko = stanowisko;
    }

    public double getPensja() {
        return pensja;
    }

    public void setPensja(double pensja) {
        this.pensja = pensja;
    }

    public double getPremia() {
        return premia;
    }

    public void setPremia(double premia) {
        this.premia = premia;
    }

    private String imie;
    private String nazwisko;
    private int wiek;
    private String pesel;
    private String username;
    private  String password;
    private String stanowisko;
    private double pensja;
    private double premia;



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pracownik gosc = (Pracownik) o;
        return getWiek() == gosc.getWiek() &&
                getPesel() == gosc.getPesel() &&
                Objects.equals(getImie(), gosc.getImie()) &&
                Objects.equals(getNazwisko(), gosc.getNazwisko());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getImie(), getNazwisko(), getWiek(), getPesel());
    }
}
