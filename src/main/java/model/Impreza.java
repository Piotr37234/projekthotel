package model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@NamedQueries({
        @NamedQuery(name = "Impreza.findAll", query = "SELECT p FROM Impreza p"),
        @NamedQuery(name = "Impreza.findbyId", query = "SELECT p FROM Impreza p WHERE p.id=:id"),


}
)


@Entity
public class Impreza {
    @Id
    @GeneratedValue(generator="Impreza_seq")
    @SequenceGenerator(name="Impreza_seq",sequenceName="Impreza_seq", allocationSize=1)
    private int id;
    private String nazwa;
    private int opis;
    private double cena;
    @ManyToOne
    private Gosc organizator;
    @ManyToOne
    private SalaImprez sala;
    @Temporal(TemporalType.TIMESTAMP)
    private Date czaspoczatkowy;
    @Temporal(TemporalType.TIMESTAMP)
    private Date czaskoncowy;




    public Date getCzaspoczatkowy() {
        return czaspoczatkowy;
    }

    public void setCzaspoczatkowy(Date czaspoczatkowy) {
        this.czaspoczatkowy = czaspoczatkowy;
    }

    public Date getCzaskoncowy() {
        return czaskoncowy;
    }

    public void setCzaskoncowy(Date czaskoncowy) {
        this.czaskoncowy = czaskoncowy;
    }






    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public Gosc getOrganizator() {
        return organizator;
    }

    public void setOrganizator(Gosc organizator) {
        this.organizator = organizator;
    }

    public SalaImprez getSala() {
        return sala;
    }

    public void setSala(SalaImprez sala) {
        this.sala = sala;
    }






    public int getOpis() {
        return opis;
    }

    public void setOpis(int opis) {
        this.opis = opis;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }





}
