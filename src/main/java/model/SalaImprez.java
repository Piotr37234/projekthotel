package model;

import javax.persistence.*;
import java.util.Date;


@NamedQueries({
        @NamedQuery(name = "SalaImprez.findAll", query = "SELECT p FROM SalaImprez p"),
        @NamedQuery(name = "SalaImprez.findbyId", query = "SELECT p FROM SalaImprez p WHERE p.id=:id"),


}
)


@Entity
public class SalaImprez {
    @Id
    @GeneratedValue(generator="SalaImprez_seq")
    @SequenceGenerator(name="SalaImprez_seq",sequenceName="SalaImprez_seq", allocationSize=1)
    private int id;

    private String nazwa;
    private double cena;
    private boolean dostepnosc;
    private int typSali;
    private int opis;








    public int getTypSali() {
        return typSali;
    }

    public void setTypSali(int typSali) {
        this.typSali = typSali;
    }


    public int getOpis() {
        return opis;
    }

    public void setOpis(int opis) {
        this.opis = opis;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public boolean isDostepnosc() {
        return dostepnosc;
    }

    public void setDostepnosc(boolean dostepnosc) {
        this.dostepnosc = dostepnosc;
    }


}
