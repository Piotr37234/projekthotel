package model;

import javax.persistence.*;


@NamedQueries({
        @NamedQuery(name = "Wiadomosc.findAll", query = "SELECT p FROM Gosc p"),
        @NamedQuery(name = "Wiadomosc.findbyId", query = "SELECT p FROM Gosc p WHERE p.id=:id"),
        @NamedQuery(name = "Wiadomosc.nadawca", query = "SELECT p FROM Wiadomosc p WHERE p.nadawca.id=:idnadawcy"),
        @NamedQuery(name = "Wiadomosc.odbiorca", query = "SELECT p FROM Wiadomosc p WHERE p.odbiorca.id=:idodbiorcy")



        //    @NamedQuery(name = "gosc.findpesel", query = "SELECT all FROM Gosc all ")

}
)


@Entity
public class Wiadomosc {


    @Id
    @GeneratedValue(generator="Wiadomosc_seq")
    @SequenceGenerator(name="Wiadomosc_seq",sequenceName="Wiadomosc_seq", allocationSize=1)
    private int id;

    @ManyToOne
    Gosc nadawca;
    private String temat;
    private String tresc;
    @ManyToOne
    Gosc odbiorca;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Gosc getNadawca() {
        return nadawca;
    }

    public void setNadawca(Gosc nadawca) {
        this.nadawca = nadawca;
    }

    public String getTemat() {
        return temat;
    }

    public void setTemat(String temat) {
        this.temat = temat;
    }

    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    public Gosc getOdbiorca() {
        return odbiorca;
    }

    public void setOdbiorca(Gosc odbiorca) {
        this.odbiorca = odbiorca;
    }











}
