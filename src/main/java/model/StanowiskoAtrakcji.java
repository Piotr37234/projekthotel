package model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;


@NamedQueries({
        @NamedQuery(name = "StanowiskoAtrakcji.findAll", query = "SELECT p FROM StanowiskoAtrakcji p"),
        @NamedQuery(name = "StanowiskoAtrakcji.findbyId", query = "SELECT p FROM StanowiskoAtrakcji p WHERE p.id=:id"),
        @NamedQuery(name = "StanowiskoAtrakcji.typ", query = "SELECT p FROM StanowiskoAtrakcji p WHERE p.typ.id=:id"),
        @NamedQuery(name = "StanowiskoAtrakcji.typ.dostepne", query = "SELECT p FROM StanowiskoAtrakcji p WHERE p.typ.id=:id AND p.dostepnosc=true")
}
)


@Entity
public class StanowiskoAtrakcji {
    @Id
    @GeneratedValue(generator="StanowiskoAtrakcji_seq")
    @SequenceGenerator(name="StanowiskoAtrakcji_seq",sequenceName="StanowiskoAtrakcji_seq", allocationSize=1)
    private int id;

    private String nazwa;
    private boolean dostepnosc;
    @ManyToOne
    private TypAtrakcji typ;




    public TypAtrakcji getTyp() {
        return typ;
    }

    public void setTyp(TypAtrakcji typ) {
        this.typ = typ;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }


    public boolean isDostepnosc() {
        return dostepnosc;
    }

    public void setDostepnosc(boolean dostepnosc) {
        this.dostepnosc = dostepnosc;
    }


}
