package model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;



@NamedQueries({
        @NamedQuery(name = "Rezerwacja.findAll", query = "SELECT p FROM Rezerwacja p"),
        @NamedQuery(name = "Rezerwacja.findbyId", query = "SELECT p FROM Rezerwacja p WHERE p.id=:id"),
        @NamedQuery(name = "Rezerwacja.FindRezerwacjeGoscia", query = "SELECT p FROM Rezerwacja p WHERE p.osoba.id=:idgosc"),
        @NamedQuery(name = "Rezerwacja.zajeteWczasie", query = "SELECT p FROM Rezerwacja p WHERE  :id_pokoju=p.pokoj.id AND (p.czaskoncowy>=:czaspoczatkowy AND :czaspoczatkowy>=p.czaspoczatkowy OR (:czaspoczatkowy>czaspoczatkowy AND :czaskoncowy<p.czaskoncowy OR  :czaspoczatkowy<czaspoczatkowy AND :czaskoncowy>p.czaskoncowy) OR p.czaskoncowy>=:czaskoncowy AND :czaskoncowy>=p.czaspoczatkowy)"),

}
)



@Entity
public class Rezerwacja {

    @Id
    @GeneratedValue(generator="Rezerwacja_seq")
    @SequenceGenerator(name="Rezerwacja_seq",sequenceName="Rezerwacja_seq", allocationSize=1)

    private int id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date czaspoczatkowy;
    @Temporal(TemporalType.TIMESTAMP)
    private Date czaskoncowy;
    private boolean rozliczona;

    @ManyToOne
    private Gosc osoba;
    @ManyToOne
    private Pokoj pokoj;


    public boolean isRozliczona() {
        return rozliczona;
    }

    public void setRozliczona(boolean rozliczona) {
        this.rozliczona = rozliczona;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCzaspoczatkowy() {
        return czaspoczatkowy;
    }

    public void setCzaspoczatkowy(Date czaspoczatkowy) {
        this.czaspoczatkowy = czaspoczatkowy;
    }

    public Date getCzaskoncowy() {
        return czaskoncowy;
    }

    public void setCzaskoncowy(Date czaskoncowy) {
        this.czaskoncowy = czaskoncowy;
    }

    public Gosc getOsoba() {
        return osoba;
    }

    public void setOsoba(Gosc osoba) {
        this.osoba = osoba;
    }

    public Pokoj getPokoj() {
        return pokoj;
    }

    public void setPokoj(Pokoj pokoj) {
        this.pokoj = pokoj;
    }







    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rezerwacja that = (Rezerwacja) o;
        return id == that.id &&
                Objects.equals(czaspoczatkowy, that.czaspoczatkowy) &&
                Objects.equals(czaskoncowy, that.czaskoncowy);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, czaspoczatkowy, czaskoncowy);
    }
}
