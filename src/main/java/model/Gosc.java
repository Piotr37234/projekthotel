package model;

import javax.persistence.*;
import java.util.Objects;




@NamedQueries({
        @NamedQuery(name = "Gosc.findAll", query = "SELECT p FROM Gosc p"),
        @NamedQuery(name = "Gosc.findbyId", query = "SELECT p FROM Gosc p WHERE p.id=:id"),
        @NamedQuery(name = "Gosc.findbyEmail", query = "SELECT p FROM Gosc p WHERE p.email=:email"),
        @NamedQuery(name = "Gosc.findbyKod", query = "SELECT p FROM Gosc p WHERE p.kod=:kod"),
        @NamedQuery(name = "Gosc.findPeselGosc", query = "SELECT p FROM Gosc p WHERE p.username=:username AND p.password=:password"),
        @NamedQuery(name = "Gosc.findLogin", query = "SELECT p FROM Gosc p WHERE p.username=:username"),
        @NamedQuery(name = "Gosc.rachunekZaRezerwacje", query = "SELECT sum(p.pokoj.cena*(day(czaskoncowy-czaspoczatkowy))) FROM Rezerwacja p WHERE p.osoba.id=:idgosc AND p.rozliczona=false"),
        @NamedQuery(name = "Gosc.zaplacZaRezerwacje", query = " UPDATE Rezerwacja p SET p.rozliczona=true WHERE p.osoba.id=:idgosc")
    //    @NamedQuery(name = "gosc.findpesel", query = "SELECT all FROM Gosc all ")

}
)


@Entity
public class Gosc {
    @Id
    @GeneratedValue(generator="Gosc_seq")
    @SequenceGenerator(name="Gosc_seq",sequenceName="Gosc_seq", allocationSize=1)

    private int id;
    private String imie;
    private String nazwisko;
    private int wiek;
    private String email;
    private String pesel;
    private String username;
    private String password;
    private double rachunek;
    private boolean rozliczony;
    private boolean admin;
    private String kod;
    private boolean aktywowane;





    public boolean isAktywowane() {
        return aktywowane;
    }

    public void setAktywowane(boolean aktywowane) {
        this.aktywowane = aktywowane;
    }



    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }


    public boolean isRozliczony() {
        return rozliczony;
    }

    public void setRozliczony(boolean rozliczony) {
        this.rozliczony = rozliczony;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public double getRachunek() {
        return rachunek;
    }

    public void setRachunek(double rachunek) {
        this.rachunek = rachunek;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Gosc gosc = (Gosc) o;
        return getWiek() == gosc.getWiek() &&
                getPesel() == gosc.getPesel() &&
                getEmail() == gosc.getEmail() &&
                Objects.equals(getImie(), gosc.getImie()) &&
                Objects.equals(getNazwisko(), gosc.getNazwisko());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getImie(), getNazwisko(), getWiek(), getPesel(),getEmail());
    }

    public Gosc() {
    }
}
